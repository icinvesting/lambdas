export default {
  type: 'object',
  properties: {
    name: { type: 'string' },
    contactNumber: { type: 'string' },
    emailAddress: { type: 'string' },
    message: { type: 'string' },
  },
  required: ['contactNumber', 'emailAddress', 'name'],
} as const;
