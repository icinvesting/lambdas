import * as FS from 'fs';
export const readFileAsync = async (filePath: string): Promise<Buffer> => {
  try {
    const data = await FS.promises.readFile(filePath);
    return Buffer.from(data);
  } catch (e) {
    throw new Error(`Error: Unable to read file at - ${filePath}`);
  }
};