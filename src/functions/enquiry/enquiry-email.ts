import { compile } from 'handlebars';
import { SendMailOptions } from 'nodemailer';

import { EmailModel } from './email.model';
import { readFileAsync } from './file-reader';
import { getImageFile, getTemplateFile } from './path-utils';

const source = 'enquiries@ironcladinvestments.co.uk';
const subject = 'New Ironclad investments enquiry';

const getBodyText = async (emailData: EmailModel): Promise<string> => {
  const inviteTextFile = await readFileAsync(getTemplateFile('enquiry.txt'));
  const templateText = compile(inviteTextFile.toString());
  const bodyText = templateText(emailData);
  return bodyText;
};

const getBodyHtml = async (emailData: EmailModel): Promise<string> => {
  const inviteHtmlTemplate = await readFileAsync(getTemplateFile('enquiry.html'));
  const templateHtml = compile(inviteHtmlTemplate.toString());
  const bodyHtml = templateHtml(emailData);
  return bodyHtml;
};

export const getEnquiryEmailOptions = async (data: EmailModel): Promise<SendMailOptions> => ({
  from: source,
  to: source,
  html: await getBodyHtml(data),
  text: await getBodyText(data),
  subject,
  attachments: [{
    cid: 'logo',
    filename: 'ic-logo.png',
    path: getImageFile('ic-logo.png')
  }]
});