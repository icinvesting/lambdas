import path from 'path';

export const getTemplateFile = (templateName: string): string => {
  return path.resolve(`src/templates/${templateName}`);
};

export const getImageFile = (imageName: string): string => {
  return path.resolve(`src/templates/img/${imageName}`);
};
