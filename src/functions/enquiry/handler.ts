
import AWS from 'aws-sdk';
import { createTransport } from 'nodemailer';

import { formatJSONResponse } from '@libs/api-gateway';
import { middyfy } from '@libs/lambda';

import { EmailModel } from './email.model';
import { getEnquiryEmailOptions } from './enquiry-email';
import { getThankYouEmailOptions } from './thank-you-email';

const transporter = createTransport({
  SES: new AWS.SES()
});

const sendEnquiryEmail = async (requestData: EmailModel) => {
  const enquiryEmail = await getEnquiryEmailOptions({ ...requestData })
  return transporter.sendMail(enquiryEmail);
};

const sendThankYouEmail = async (email: string, name: string) => {
  const thankYouEmail = await getThankYouEmailOptions(email, name)
  return transporter.sendMail(thankYouEmail);
};

const sendEmails = async (requestData: EmailModel) => {
  await sendEnquiryEmail(requestData);
  await sendThankYouEmail(requestData.emailAddress, requestData.name);
};

const handleEnquiry = async (event) => {
  try {
    const emailModel: EmailModel = event?.body ? event.body : event;
    await sendEmails(emailModel)
    return Promise.resolve(formatJSONResponse({ success: true }));
  } catch (error) {
    return Promise.resolve(formatJSONResponse({ success: false }));
  }
};

export const main = middyfy(handleEnquiry);
