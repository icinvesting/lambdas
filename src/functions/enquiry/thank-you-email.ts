import { compile } from 'handlebars';
import { SendMailOptions } from 'nodemailer';

import { readFileAsync } from './file-reader';
import { getImageFile, getTemplateFile } from './path-utils';

const source = 'enquiries@ironcladinvestments.co.uk';
const subject = 'Your Ironclad investments enquiry';

const getBodyText = async (name: string): Promise<string> => {
  const inviteTextFile = await readFileAsync(getTemplateFile('thank-you.txt'));
  const templateText = compile(inviteTextFile.toString());
  const bodyText = templateText({ name });
  return bodyText;
};

const getBodyHtml = async (name: string): Promise<string> => {
  const inviteHtmlTemplate = await readFileAsync(getTemplateFile('thank-you.html'));
  const templateHtml = compile(inviteHtmlTemplate.toString());
  const bodyHtml = templateHtml({ name });
  return bodyHtml;
};

export const getThankYouEmailOptions = async (emailAddress: string, name: string): Promise<SendMailOptions> => ({
  from: source,
  to: emailAddress,
  html: await getBodyHtml(name),
  text: await getBodyText(name),
  subject,
  attachments: [{
    cid: 'logo',
    filename: 'ic-logo.png',
    path: getImageFile('ic-logo.png')
  }, {
    cid: 'hero',
    filename: 'email-hero.jpg',
    path: getImageFile('email-hero.jpg')
  }]
});