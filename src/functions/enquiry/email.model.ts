export interface EmailModel {
  contactNumber: string;
  emailAddress: string;
  message: string;
  name: string;
}